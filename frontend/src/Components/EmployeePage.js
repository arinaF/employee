import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Table from 'react-bootstrap/Table';
import { css } from "@emotion/core";
import ClipLoader from "react-spinners/ClipLoader";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: blue;
`;

class EmployeePage extends Component {

    constructor() {
        super();
        this.state = {
            employee: [],
            loginTimeStamps: [],
            isLoading: false
        };
        this.getEmployee = this.getEmployee.bind(this);
    }

    componentDidMount() {
        this.getEmployee();
    }

    getEmployee(){
        const { id } = this.props.match.params;
            this.setState({isLoading: true});
              fetch(`/api/employees/${id}`)
                .then(response => response.json())
                .then(data => this.setState({employee: data, loginTimeStamps: data.loginTimeStamps, isLoading: false}));
   }

    render() {
        const {employee, loginTimeStamps, isLoading} = this.state;
        return (
        <div style={{margin: 10}}>
            <ClipLoader
              css={override}
              size={40}
              color={"#0000FF"}
              loading={isLoading}
            />
            <div id="page-content">
            <h2 class="display-8">Employee</h2>
            <dl class="row">
                <dt class="col-sm-3">First Name</dt>
                <dd class="col-sm-9">{employee.firstName}</dd>

                <dt class="col-sm-3">Last Name</dt>
                <dd class="col-sm-9">{employee.lastName}</dd>

                <dt class="col-sm-3">Phone</dt>
                <dd class="col-sm-9">{employee.phone}</dd>

                <dt class="col-sm-3">Login</dt>
                <dd class="col-sm-9">{employee.login}</dd>
            </dl>
            <Table bordered>
                <thead>
                    <tr>
                        <th>Login Timestamp</th>
                    </tr>
                </thead>
                    {loginTimeStamps.slice(0, 10).map((log: Log, i) =>
                <tbody>
                    <tr>
                        <td>{log.loginTimestamp}</td>
                    </tr>
                </tbody>
                )}
            </Table>
            </div>
            <div id="page-footer">
            </div>
        </div>
        );
    }
}

export default EmployeePage;
