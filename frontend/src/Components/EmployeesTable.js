import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Table from 'react-bootstrap/Table';
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';
import { css } from "@emotion/core";
import ClipLoader from "react-spinners/ClipLoader";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: blue;
`;

class EmployeesTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            currentPage: 1,
            visible: 10,
            isLoading: false,
            totalCount: 0
        };
        this.onPageChange = this.onPageChange.bind(this);
        this.getEmployees = this.getEmployees.bind(this);
        this.getTotalCount = this.getTotalCount.bind(this);
    }

    componentDidMount() {
        this.getEmployees();
        this.getTotalCount();
    }

    getEmployees(){
        this.setState({isLoading: true});
          fetch(`/api/employees?page=${this.state.currentPage}&size=${this.state.visible}`)
            .then(response => response.json())
            .then(data => this.setState({employees: data,
                                        isLoading: false}));
    }

    getTotalCount(){
        this.setState({isLoading: true});
          fetch(`/api/totalcounts/employees`)
            .then(response => response.json())
            .then(data => this.setState({totalCount: data,
                                        isLoading: false}));
    }

    onPageChange = (page) => {
        this.state.currentPage = page;
        this.getEmployees();
    }

    render() {
        const {employees, isLoading, totalCount, currentPage} = this.state;
        return (
            <div style={{margin: 10}} id="container">
                <ClipLoader
                  css={override}
                  size={40}
                  color={"#0000FF"}
                  loading={isLoading}
                />
                <div id="table">
                <h2 class="display-8">Employees</h2>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                        </tr>
                    </thead>
                        {employees.map((employee: Employee, i) =>
                    <tbody>
                        <tr>
                            <td><Link to={`/employees/${employee.id}`}>{employee.firstName}</Link></td>
                            <td><Link to={`/employees/${employee.id}`}>{employee.lastName}</Link></td>
                        </tr>
                    </tbody>
                    )}
                </Table>
                <br/>
                    <Pagination
                      onChange={this.onPageChange}
                      current={currentPage}
                      total={totalCount}
                      showLessItems
                      showTitle={false}
                    />
                 <br />
                 </div>
            </div>
        );
    }
}

export default EmployeesTable;
