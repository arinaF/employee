import React, { Component } from "react";
import EmployeesTable from "./Components/EmployeesTable";
import EmployeePage from './Components/EmployeePage';
import { Route, BrowserRouter as Router } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <Router>
        <Route exact path="/employees/" component={EmployeesTable} />
        <Route exact path="/employees/:id" component={EmployeePage} />
      </Router>
    );
  }
}

export default App;