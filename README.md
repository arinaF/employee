**Introduction**

It is a simple application, that shows a table with all employees and gives a possibility to view details of every employee. The application has two parts: backend and frontend. The backend part works like REST API service with JSON as response. The frontend part is simple React application which displays received JSON in appropriate way.  
Technologies used: Java 8, Spring Boot, Maven, H2 Database, React

**Running app**  

To start backend part use command `mvn spring-boot:run`  
To start frontend part go to `/frontend` directory and use commands:  
`npm start`

The DB table `EMPLOYEES` is fullfiled automatically when the backend starts. To insert data into DB table `EMPLOYEES_LOGINS` it is need to execute the script `inser_logins.sql` which could be found in `backend/src/main/resources` folder. 

To execute the mentioned script please open H2 database console using the link:    
http://localhost:8080/h2  
Credentials can be found in `backend/src/main/resources/application.properties` file.  

Application link:  
http://localhost:3000/employees