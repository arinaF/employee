DROP ALIAS IF EXISTS INSERT_LOGINS;

CREATE ALIAS INSERT_LOGINS AS $$
void insertLogins(java.sql.Connection con) throws Exception {
int count = 100000;
while(count >=0){
Statement statement = con.createStatement();
        statement.executeUpdate("insert into employees_logins(employeeid, logintimestamp) values ((select (rand()*9999+1)::int), (select  TIMESTAMPADD(SECOND, (select (rand()*31535999+1)::int), '2019-03-28 00:00:000')))");

		count--;
		}
}
$$;

SELECT INSERT_LOGINS();