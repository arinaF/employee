package lv.visma.employees.dto;

public interface EmployeeSimpleDTO {
    public int getId();
    public String getFirstName();
    public String getLastName();
}
