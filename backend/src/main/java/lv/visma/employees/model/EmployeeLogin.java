package lv.visma.employees.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "employees_logins")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EmployeeLogin {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @Column(name = "employeeid")
    private long employeeId;

    @NotNull
    @Column(name = "logintimestamp")
    private Date loginTimestamp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public Date getLoginTimestamp() {
        return loginTimestamp;
    }

    public void setLoginTimestamp(Date loginTimestamp) {
        this.loginTimestamp = loginTimestamp;
    }
}
